import React from "react";
import { Link } from "react-router-dom";

import Footer from "./components/Footer";

import "./css/style.css";

function Navigation() {
  return (
    <nav>
      <h1 className="title">Product List</h1>
      <div className="end">
        <Link to="addproduct" className="btn btn-primary mr-1">
          ADD
        </Link>
        <button id="delete-product-btn" className="btn btn-danger">
          MASS DELETE
        </button>
      </div>
    </nav>
  );
}

function Box(props) {
  return (
    <div className="grid-item-4">
      <div className="box">
        <div className="checkbox">
          <input type="checkbox" name={props.sku} id={props.sku} />
        </div>
        <div className="infobox">
          <p>{props.sku}</p>
          <p>{props.name}</p>
          <p>{props.price} $</p>
        </div>
      </div>
    </div>
  );
}

function App() {
  return (
    <div className="app">
      <Navigation />
      <hr />
      <div className="grid-container-4">
        <Box sku="GIO-BLINK-DVD" name="Blink-182 Album" price="25" />
        <Box sku="GIO-ADTR-CD" name="A Day To Remember Album" price="25" />
        <Box sku="GIO-ADTR-CD" name="A Day To Remember Album 3" price="25" />
        <Box sku="GIO-RED-Shirt" name="T-Shirt Blank Red" price="15" />
        <Box sku="GIO-BLINK-DVD" name="Blink-182 Album" price="25" />
        <Box sku="GIO-ADTR-CD" name="A Day To Remember Album" price="25" />
        <Box sku="GIO-ADTR-CD" name="A Day To Remember Album" price="25" />
        <Box sku="GIO-RED-Shirt" name="T-Shirt Blank Red" price="15" />
      </div>
      <hr />
      <Footer />
    </div>
  );
}

export default App;
