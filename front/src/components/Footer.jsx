import React from "react";

function Footer() {
  return (
    <footer>
      <h2 className="title-2 text-center p-2">Scandiweb Test assignment</h2>
    </footer>
  );
}

export default Footer;
