import React, { useState } from "react";
import { Link } from "react-router-dom";

import Footer from "../components/Footer";

function Navigation() {
  return (
    <nav>
      <h1 className="title">Product Add</h1>
      <div className="end">
        <button className="btn btn-primary mr-1">Save</button>
        <Link to="/" className="btn btn-danger">
          Cancel
        </Link>
      </div>
    </nav>
  );
}

function DVD() {
  return (
    <div className="grid-container">
      <div className="grid-item-1">
        <label htmlFor="size" className="form-label">Size (MB)</label>
        <input type="number" name="size" id="size" className="form-input is-auto mb-3 ml-1 mt-1" required autocomplete="false" v-model="size" />
      </div>
      <div className="grid-item-2">
        <div className="text-center">
          <p>
            Please enter the size of the DVD in MBs
            <br />
            <small>
              If you need a conversor we recomend this simple one.
              <a href="https://www.unitconverters.net/data-storage/gb-to-mb.htm" target="_blank">Click here</a>
            </small>
          </p>
        </div>
      </div>
    </div>
  );
}

function Furniture() {
  return (
    <div className="grid-container">
      <div className="grid-item-1">
        <label htmlFor="height" className="form-label">Height (cm)</label>
        <input type="number" name="height" id="height" className="form-input is-auto mb-1 ml-1 mt-1" required autocomplete="false" v-model="height" /> <br />

        <label htmlFor="width" className="form-label">Width (cm)</label>
        <input type="number" name="width" id="width" className="form-input is-auto mb-1 ml-1 mt-1" required autocomplete="false" v-model="width" /> <br />

        <label htmlFor="length" className="form-label">Length (cm)</label>
        <input type="number" name="length" id="length" className="form-input is-auto mb-1 ml-1 mt-1" required autocomplete="false" v-model="length" /> <br />
      </div>
      <div className="grid-item-2">
        <div className="text-center">
          <p>
            Please, provide dimensions of the furniture!
            <br />
            Be sure the dimensions informed is in <strong>centimetres</strong>.
            <br /> <br />
            <small>
              If you need a conversor we recomend this simple one.
              <a href="https://www.unitconverters.net/length/inches-to-cm.htm" target="_blank">Click here</a>
            </small>
          </p>
        </div>
      </div>

    </div>
  );
}

function Book() {
  return (
    <div className="grid-container">
      <div className="grid-item-1">
        <label htmlFor="weight" className="form-label">Weight (Kg)</label>
        <input type="number" name="weight" id="weight" className="form-input is-auto mb-3 ml-1 mt-1" required autocomplete="false" v-model="weight" />
      </div>
      <div className="grid-item-2">
        <div className="text-center">
          <p>
            Please enter the weight of the book in Kilograms.
            <br /> <br />
            <small>
              If you need a conversor we recomend this simple one.
              <a href="https://www.unitconverters.net/weight-and-mass/lbs-to-kg.htm" target="_blank">Click here</a>
            </small>
          </p>
        </div>
      </div>

    </div>
  )
}


function GeneralForm() {
  let select;
  const [sku, setSku] = useState("");
  const [name, setName] = useState("");
  const [price, setPrice] = useState(0);
  const [type, setType] = useState("");

  if (type === "DVD") {
    select = <DVD />;
  }
  if (type === "Furniture") {
    select = <Furniture />;
  }
  if (type === "Book") {
    select = <Book />;
  }


  console.log(sku);
  console.log(name);
  console.log(price);

  return (
    <>
      <form action="/api/post" method="post" id="product_form">
        <label htmlFor="sku" className="form-label">SKU</label>
        <input type="text" name="sku" id="sku" className="form-input mb-3 mt-1" required autocomplete="false" onChange={(e) => setSku(e.target.value)} />

        <label htmlFor="name" className="form-label">Name</label>
        <input type="text" name="name" id="name" className="form-input mb-3 mt-1" required autocomplete="false" onChange={(e) => setName(e.target.value)} />

        <label htmlFor="price" className="form-label">Price ($)</label>
        <input type="number" name="price" id="price" className="form-input mb-3 mt-1" required autocomplete="false" value={price} onChange={(e) => setPrice(e.target.value)} />

        <label htmlFor="productType">Type Switcher</label>
        <select id="productType" name="productType" className="mb-2 ml-2 p-1" required onChange={(e) => setType(e.target.value)}>
          <option value="">Type selector</option>
          <option id="DVD" value="DVD">DVD</option>
          <option id="Furniture" value="Furniture">Furniture</option>
          <option id="Book" value="Book">Book</option>
        </select>

        {select}


      </form>
    </>
  );
}

function Addproduct() {
  return (
    <>
      <Navigation />
      <hr />
      <GeneralForm />
      <hr />
      <Footer />
    </>
  );
}

export default Addproduct;
