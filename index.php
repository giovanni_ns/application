<?php

$request = $_SERVER['REQUEST_URI'];

switch ($request) {
    case '/':
        require __DIR__ . '/php/views/index.php';
        break;
    case '':
        require __DIR__ . '/php/views/index.php';
        break;

    case '/addproduct':
        require __DIR__ . '/php/views/addproduct.php';
        break;

    case '/api/get':
        require __DIR__ . '/php/api/get.php';
        break;

    case '/api/post':
        require __DIR__ . '/php/api/post.php';
        break;

    default:
        http_response_code(404);
        require __DIR__ . '/php/views/404.php';
        break;
}
