<?php

class Get
{

    private $json;

    function __construct()
    {
        require __DIR__ . "/../database/connect.php";

        $stmt = $conn->prepare("SELECT * FROM product");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $this->json = json_encode($result);
    }

    public function getFetch()
    {
        echo $this->json;
    }
}

$g = new Get();
$g->getFetch();
