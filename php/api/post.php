<?php

use products\Book\Book;
use products\Dvd\Dvd;
use products\Furniture\Furniture;

require_once __DIR__ . "/../products/dvd.php";
require_once __DIR__ . "/../products/furniture.php";
require_once __DIR__ . "/../products/book.php";

class Post
{
    private $sku;
    private $name;
    private $price;
    private $productType;

    private function postDvd()
    {
        $dvd = new Dvd();
        $dvd->postDb($this->sku, $this->name, $this->price, $this->productType);
    }

    private function postFurniture()
    {
        $furn = new Furniture();
        $furn->postDb($this->sku, $this->name, $this->price, $this->productType);
    }

    private function postBook()
    {
        $book = new Book();
        $book->postDb($this->sku, $this->name, $this->price, $this->productType);
    }

    function __construct($type)
    {
        $this->sku = $_POST['sku'];
        $this->name = $_POST['name'];
        $this->price = $_POST['price'];
        $this->productType = $type;

        $type == "DVD" ? $this->postDvd() : false;
        $type == "Furniture" ? $this->postFurniture() : false;
        $type == "Book" ? $this->postBook() : false;
    }
}

$productType = $_POST['productType'];
$post = new Post($productType);
