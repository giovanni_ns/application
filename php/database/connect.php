<?php
require_once 'env.php';

$servername = "localhost";
$username = getenv("USER");
$password = getenv("PASSWORD");
$dbname = getenv("DATABASE");

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
