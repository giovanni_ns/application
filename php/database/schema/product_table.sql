CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `sku` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `hwl` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`hwl`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

GRANT all privileges ON DBNAME.* TO 'USER'@'%' identified BY 'PASSWORD';