<?php

namespace products\Dvd;

use Product\Product as Product;

require_once __DIR__ . '/product.php';

class Dvd extends Product
{

    private $size;
    
    public function getType($type)
    {
        echo "a";
    }

    public function postDb($sku, $name, $price, $type)
    {
        $this->size = $_POST['size'];
        
        require_once __DIR__ . '/../database/connect.php';
        $sql = "INSERT INTO product (sku, name, price, type, size, hwl) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$sku, $name, floatval($price), $type, intval($this->size), json_encode("")]);
        // echo "$this->size";
        // echo "$sku";
        // echo "$name";
        // echo "$price";
        // echo "$type";
        echo "success";
    }
}
