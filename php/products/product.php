<?php

namespace Product;

abstract class Product
{
    private $sku;
    private $name;
    private $price;
    private $type;

    /**
     * Function to get only the $type
     * 
     * @param string $type Define the type wich will be fecth
     * @return json 
     */
    abstract protected function getType($type);

    /**
     * Function to post the data into the database
     * 
     * @param string $sku The unic SKU of the product
     * @param string $name The name displayed of the product
     * @param int $price The price of the product
     * @param string $type The type of the product defines what was be save in
     * the database. Depends the type can be enter more than one paramters
     * @return json
     */
    abstract protected function postDb($sku, $name, $price, $type);
}