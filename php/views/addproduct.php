<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://unpkg.com/vue@3"></script>
    <title>Application</title>
</head>

<body>
    <div id="addproduct-app">

        <nav>
            <h1 class="title">Product Add</h1>
            <div class="end">
                <button @click="uploadAxios" class="btn btn-primary mr-1">Save</button>
                <a href="/" class="btn btn-danger">Cancel</a>
            </div>
        </nav>
        <hr>

        <main>
            <div class="grid-container">
                <div class="grid-item-1 p-3 m-2">
                    <form action="/api/post" method="post" id="product_form">
                        <label for="sku" class="form-label">SKU</label>
                        <input type="text" name="sku" id="sku" class="form-input mb-3 mt-1" required autocomplete="false" v-model="sku"/>

                        <label for="name" class="form-label">Name</label>
                        <input type="text" name="name" id="name" class="form-input mb-3 mt-1" required autocomplete="false" v-model="name"/>

                        <label for="price" class="form-label">Price ($)</label>
                        <input type="number" name="price" id="price" class="form-input mb-3 mt-1" required autocomplete="false" v-model="price"/>

                        <label for="productType">Type Switcher</label>
                        <select id="productType" name="productType" class="mb-2 ml-2 p-1" required v-model="type">
                            <option id="DVD" value="DVD">DVD</option>
                            <option id="Furniture" value="Furniture">Furniture</option>
                            <option id="Book" value="Book">Book</option>
                        </select>

                        <div v-if="type == 'DVD'">
                            <div class="grid-container">
                                <div class="grid-item-1">

                                    <label for="size" class="form-label">Size (MB)</label>
                                    <input type="number" name="size" id="size" class="form-input is-auto mb-3 ml-1 mt-1" required autocomplete="false" v-model="size">

                                </div>
                                <div class="grid-item-2">
                                    <div class="text-center">
                                        <p>
                                            Please enter the size of the DVD in MBs
                                            <br>
                                            <small>
                                                If you need a conversor we recomend this simple one. <a href="https://www.unitconverters.net/data-storage/gb-to-mb.htm" target="_blank">Click here</a>
                                            </small>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div v-else-if="type == 'Furniture'">
                            <div class="grid-container">
                                <div class="grid-item-1">
                                    <label for="height" class="form-label">Height (cm)</label>
                                    <input type="number" name="height" id="height" class="form-input is-auto mb-1 ml-1 mt-1" required autocomplete="false" v-model="height"> <br>

                                    <label for="width" class="form-label">Width (cm)</label>
                                    <input type="number" name="width" id="width" class="form-input is-auto mb-1 ml-1 mt-1" required autocomplete="false" v-model="width"> <br>

                                    <label for="length" class="form-label">Length (cm)</label>
                                    <input type="number" name="length" id="length" class="form-input is-auto mb-1 ml-1 mt-1" required autocomplete="false" v-model="length"> <br>
                                </div>
                                <div class="grid-item-2">
                                    <div class="text-center">
                                        <p>
                                            Please, provide dimensions of the furniture!
                                            <br>
                                            Be sure the dimensions informed is in <strong>centimetres</strong>.
                                            <br>
                                            <br>
                                            <small>
                                                If you need a conversor we recomend this simple one. <a href="https://www.unitconverters.net/length/inches-to-cm.htm" target="_blank">Click here</a>
                                            </small>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div v-else-if="type == 'Book'">
                            <div class="grid-container">
                                <div class="grid-item-1">

                                    <label for="weight" class="form-label">Weight (Kg)</label>
                                    <input type="number" name="weight" id="weight" class="form-input is-auto mb-3 ml-1 mt-1" required autocomplete="false" v-model="weight">

                                </div>
                                <div class="grid-item-2">
                                    <div class="text-center">

                                        <p>
                                            Please enter the weight of the book in Kilograms.
                                            <br>
                                            <br>
                                            <small>
                                                If you need a conversor we recomend this simple one. <a href="https://www.unitconverters.net/weight-and-mass/lbs-to-kg.htm" target="_blank">Click here</a>
                                            </small>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <button type="submit">asd</button>
                        <div v-else></div>

                        <!-- 
                        - Product type-specific attribute
                        - Size input field (in MB) for DVD-disc should have an ID: #size
                        - Weight input field (in Kg) for Book should have an ID: #weight
                        - Each from Dimensions input fields (HxWxL) for Furniture should have an appropriate ID:
                            - Height - #height
                            - Width - #width
                            - Length - #length
                    -->

                    </form>
                </div>
                <div class="grid-item-2 p-3 m-2">
                    <h3 class="text-center">Preview -- Debug</h3>
                    <div class="text-center">
                        {{sku}}<br>
                        {{name}}<br>
                        ${{price}}<br>
                        {{type}}<br>
                        {{size}} (MB)<br>
                        {{height}} (cm)<br>
                        {{width}} (cm)<br>
                        {{length}} (cm)<br>
                        {{weight}} (Kg)<br>
                    </div>
                </div>

            </div>
        </main>

        <footer>
            <hr>
            <h2 class="title-2 text-center p-2">Scandiweb Test assignment</h2>
        </footer>
    </div>

    <!-- Vue.js -->
    <script src="./js/addproduct.js"></script>
</body>

</html>