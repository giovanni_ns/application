<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://unpkg.com/vue@3"></script>
    <title>Application</title>
</head>

<body>
    <div id="main">

        <nav>
            <h1 class="title">Product List</h1>
            <div class="end">
                <a href="/addproduct" class="btn btn-primary mr-1">ADD</a>
                <button id="delete-product-btn" class="btn btn-danger">MASS DELETE</button>
            </div>
        </nav>
        <hr>

        <main>
            {{x}}
        </main>

        <footer>
            <hr>
            <h2 class="title-2 text-center p-2">Scandiweb Test assignment</h2>
        </footer>
    </div>

    <!-- Vue.js -->
    <script src="/js/index.js"></script>
</body>

</html>